package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.dao.AlbumDAO;
import db.dao.ArtistDAO;
import db.models.Album;
import db.models.Artist;

@WebServlet("/artist")
public class ArtistServlet extends HttpServlet {

    private ArtistDAO artistDao = new ArtistDAO();
    private AlbumDAO albumDao = new AlbumDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        long id = Long.parseLong(request.getParameter("id"));
        Artist artist = artistDao.findArtist(id);
        List<Album> albums = albumDao.findAlbumsByArtist(artist);

        request.setAttribute("artist", artist);
        request.setAttribute("albums", albums);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/artist.jsp");
        dispatcher.include(request, response);
    }
}
