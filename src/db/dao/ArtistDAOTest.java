package db.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import db.models.Artist;

public class ArtistDAOTest {

    private ArtistDAO artistDao;

    @Before
    public void setUp() {
        artistDao = new ArtistDAO();
    }

    @Test
    public void findAllArtistsReturnsArtists() {
        List<Artist> artists = artistDao.findAllArtists();
        assertFalse(artists.isEmpty());
    }

    @Test
    public void firstArtistHasCorrectName() {
        Artist first = artistDao.findAllArtists().get(0);
        assertEquals("A Cor Do Som", first.getName());
    }
}
