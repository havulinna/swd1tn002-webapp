package db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.ChinookDatabase;
import db.models.Album;
import db.models.Artist;

public class AlbumDAO {
    private final String SELECT_BY_ID_SQL = "SELECT AlbumId, Title, ArtistId FROM Album WHERE AlbumId = ? ORDER BY Title ASC";
    private final String SELECT_BY_ARTIST_SQL = "SELECT AlbumId, Title, ArtistId FROM Album WHERE ArtistId = ? ORDER BY Title ASC";

    private ChinookDatabase database = new ChinookDatabase();

    public Album findAlbum(long id) {
        Connection connection = database.connect();
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            statement = connection.prepareStatement(SELECT_BY_ID_SQL);
            statement.setLong(1, id);

            result = statement.executeQuery();
            if (result.next()) {
                return convertToAlbum(result);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            database.closeAll(result, statement, connection);
        }
    }

    public List<Album> findAlbumsByArtist(Artist artist) {
        List<Album> albums = new ArrayList<>();
        Connection connection = database.connect();
        PreparedStatement statement = null;
        ResultSet results = null;

        try {
            statement = connection.prepareStatement(SELECT_BY_ARTIST_SQL);
            statement.setLong(1, artist.getId());
            results = statement.executeQuery();
            while (results.next()) {
                albums.add(convertToAlbum(results));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            database.closeAll(results, statement, connection);
        }
        return albums;
    }

    private Album convertToAlbum(ResultSet results) throws SQLException {
        long id = results.getLong("AlbumId");
        String title = results.getString("Title");
        long artistId = results.getLong("ArtistId");
        return new Album(id, artistId, title);
    }
}
