package db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.ChinookDatabase;
import db.models.Artist;

public class ArtistDAO {

    private ChinookDatabase database = new ChinookDatabase();

    public Artist findArtist(long id) {
        Connection connection = database.connect();
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement("SELECT * FROM Artist WHERE ArtistId = ?");
            statement.setLong(1, id);
            result = statement.executeQuery();

            if (result.next()) {
                String name = result.getString("Name");
                return new Artist(id, name);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            database.closeAll(result, statement, connection);
        }
    }

    public List<Artist> findAllArtists() {
        Connection connection = database.connect();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Artist> artists = new ArrayList<Artist>();
        try {
            statement = connection.prepareStatement("SELECT * FROM Artist ORDER BY Name ASC");
            result = statement.executeQuery();

            while (result.next()) {
                long id = result.getLong("ArtistId");
                String name = result.getString("Name");
                Artist a = new Artist(id, name);
                artists.add(a);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            database.closeAll(result, statement, connection);
        }
        return artists;
    }

    public void storeArtist(Artist artist) {
        Connection connection = database.connect();
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            statement = connection.prepareStatement("INSERT INTO Artist (Name) VALUES (?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, artist.getName());
            statement.executeUpdate();
            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                long id = generatedKeys.getLong(1);
                artist.setId(id);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            database.closeAll(generatedKeys, statement, connection);
        }
    }
}
