package db.models;

public class Album {

    private long id;
    private long artistId;
    private String title;

    public Album(long artistId, String title) {
        this.artistId = artistId;
        this.title = title;
    }

    public Album(long id, long artistId, String title) {
        this.id = id;
        this.artistId = artistId;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getArtistId() {
        return artistId;
    }

    public void setArtistId(long artistId) {
        this.artistId = artistId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return String.format("Album %d: %s", getId(), getTitle());
    }
}
