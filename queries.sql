SELECT * FROM Artist WHERE Name LIKE "%bill%" ORDER BY Name ASC;

SELECT AlbumId, Title, ArtistId FROM Album WHERE ArtistId = 1;

INSERT INTO Album (Title, ArtistId) VALUES (?, ?)
UPDATE Album SET Title = ? WHERE AlbumId = ?

SELECT TrackId, Name, AlbumId, Composer FROM Track WHERE AlbumId = 1;

SELECT TrackId, Track.Name, AlbumId, Composer, Genre.Name AS Genre, MediaType.Name AS MediaType 
FROM Track 
LEFT JOIN MediaType ON Track.MediaTypeId = MediaType.MediaTypeId 
LEFT JOIN Genre ON Track.GenreId = Genre.GenreId 
GROUP BY Track.TrackId;