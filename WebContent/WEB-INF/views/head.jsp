<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <style>
        html, body {
            font-family: arial, sans-serif;
        }
    </style>
    <title><c:out value="${ param.title }" /> - My Record Store</title>
</head>