<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="head.jsp">
    <jsp:param value="${ artist.getName() }" name="title" />
</jsp:include>
<body>
    <h1>${ artist.getId() }: <c:out value="${ artist.getName() }" /></h1>
    <ul>
        <c:forEach items="${ albums }" var="album">
            <li><c:out value="${ album.getTitle() }" /></li>
        </c:forEach>
    </ul>
</body>
</html>
